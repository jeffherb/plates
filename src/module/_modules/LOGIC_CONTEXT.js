import {
    BETWEEN,
    CHOICE,
    EMPTY,
    LAZY,
    LETTERS,
    LITERAL,
    MANY,
    NODE,
    REG_EXP,
    OPTIONAL,
    SEQUENCE,
    THUNK
} from '../lib/parser';

const LOGIC_QAULITFIER = BETWEEN(
    LITERAL('{{'),
    LITERAL('}}')
);

// Logic Context Parser
// This combinatior is used to generate the ast of a context value reference
//
/* LOGIC CONTEXT PARSER
 * ====================
 * This combinatior is used to generate the ast of a context value reference
 * Example:
 * {{this.checked}}
 * Turns into:
 * {
 *  "type": "context",
 *  "node": "logic",
 *  "value" "checked"
 * }
 **/
const LOGIC_CONTEXT_PARSER = LOGIC_QAULITFIER(
    SEQUENCE([
        OPTIONAL(EMPTY),
        REG_EXP(/[a-zA-Z\.]+/)
    ])
).map(oResult => {

    let oNode = {
        type: "context",
        node: "logic",
        value: null
    };

    var sContextString = oResult.vResult;

    if (sContextString && sContextString.length) {

        if (Array.isArray(sContextString)) {
            sContextString = sContextString[0];
        }

        if (sContextString.startsWith("this.")) {

            sContextString = sContextString.slice(5);
        }

        oNode.value = sContextString;

        return oNode;
    }
    else {

        return null;
    }   
});