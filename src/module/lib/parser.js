/**
 * fGetContent
 * Returns everything to the right side side of the current source index (oState.iIndex) from the source string (oState.sSource). 
 * @param {object} oState Parser state object
 */
function fGetContent (oState) {

    // Get all the content that has no yet been parsed. Start from the last defined state index
    let sContent = oState.sSource.slice(oState.iIndex);

    // If we have length (contents), return them
    if (sContent.length) {
        return sContent;
    }

    // We have no content, set the state flag to be EoF (End of File);
    oState.bEoF = true;

    // Simply return nothing.
    return null;
};

/**
 * fUpdateState
 * Create a new state object (oState) based off the original, but sets the parser result (oState.vResult) 
 * @param {object}      oState      Parser state object
 * @param {varies}      vResult     The results of the provided parsers
 * @param {interger}    iMoveIndex   Indicates the length to increate index by
 */
function fUpdateState (oState, vResult, iMoveIndex) {

    // Create a new copy of the state object, but set the vResult to what was returned from the parser
    let oNewState = {
        ...oState,
        vResult: vResult
    };

    // increment the state index based on the vResult value itself, or based on the manually computed result
    if (!iMoveIndex) {
        oNewState.iIndex += vResult.length;
    }
    else {
        oNewState.iIndex += iMoveIndex;
    }

    // Check to see if the last parser result has brought the process to the end of the current source
    if (oNewState.sSource.length === oNewState.iIndex) {
        oNewState.bEoF = true;
    }

    return oNewState
};

/**
 * fUpdateResult
 * Creates a new state object but does not advance the actual state.
 * @param {object}      oState      Parser state object
 * @param {varies}      vResult     The results of the provided parsers
 */
function fUpdateResult (oState, vResult) {

    let oNewState = {
        ...oState,
        vResult: vResult
    }

    return oNewState;

};

function fErrorState (oState, sError, bFatel) {

    let oErrorState = {
        ...oState,
        oError: {
            bError: true,
            sError: sError
        }
    }

    if (bFatel) {

        throw new Error(oErrorState.oError.sError);
    }

    return oErrorState;
};

/**
 * fCloneRegExp
 * Creates a new state object but does not advance the actual state.
 * @param {regular exp} reOriginal      Original Regular Expression
 */
function fCloneRegExp (reOriginal, aSetFlags) {

    let sPattern = reOriginal.source;
    let sFlags = "";

    if (reOriginal.global) {
        sFlags += "g";
    }

    if (reOriginal.ignoreCase) {
        sFlags += "i";
    }

    if (reOriginal.multiline) {
        sFlags += "m";
    }

    if (aSetFlags && aSetFlags.length) {

        for (let s = 0, sLen = aSetFlags.length; s < sLen; s++) {

            if (sFlags.indexOf(aSetFlags[s]) === -1) {
                sFlags += aSetFlags[s];
            }

        }

    }

    return new RegExp(sPattern, sFlags);

}

class Parser {

    constructor(fTransformer) {
        this.fTransformer = fTransformer;
    }

    // Executs the provided fTranformer which is the provided Parser rule engine
    // In this class, the rules are part of the plates.js file.
    run(sSource) {

        let oInitalState = {
            sSource: sSource,
            iIndex: 0,
            oError: {
                bError: false,
                sError: null
            },
            bEoF: false
        };

        return this.fTransformer(oInitalState);
    }

    // Map intercepts the results of a fTransfomer parser so the developer
    // can define more specific output.
    map(fMap, bReturnState = false) {

        // Return the results of a new Parser based on the state being passed in from the previous call
        return new Parser(oState => {

            // Create a new state from the provided start state
            const oNextState = this.fTransformer(oState);

            if (oNextState.oError.bError) {
                return oNextState;
            }

            if (bReturnState) {
                return fUpdateResult(oNextState, fMap(oNextState));
            }

            return fUpdateResult(oNextState, fMap(oNextState));
        });

    }

}

/**
 * EMPTY
 * Parser check to see if the provided source contains any empty or blank space at the begining of the string.
 * an unlimitted number of blank spaces are detected.
 * 
 * // Parser Parameters
 * @param {object} oState Parser state object
 */
const EMPTY = new Parser(oState => {

    // Verify we have a state object and that state object is not current in "error" or at the end of the file
    if (oState && (oState.oError.bError || oState.bError)) {
        return oState;
    }

    // Define a regular expression that accepts just blank spaces
    const RE_BLANK_CHARACTERS = /[\s]+/;

    if (RE_BLANK_CHARACTERS.lastIndex !== 0) {
        RE_BLANK_CHARACTERS.lastIndex = 0;
    }

    // Get the contents
    let sContent = fGetContent(oState);

    // Check to see if we have contents to test
    if (sContent !== null) {

        let oEmptyResults = RE_BLANK_CHARACTERS.exec(sContent);

        if (oEmptyResults && oEmptyResults[0] && oEmptyResults.index === 0) {

            return fUpdateState(oState, null, oEmptyResults[0].length);
        }
        
        return fErrorState(oState, `EMPTY: Was expecting blank characers but found: ${sContent.slice(0, 20)}`);
    }

    return fErrorState(oState, `EMPTY: Unexpected End of File`, true);
});

/**
 * LETTERS
 * Parser check to see if the provided source contains letters starting at the beginnig of the string.
 * an unlimitted number of letters are allowed.
 * 
 * // Parser Parameters
 * @param {object} oState Parser state object
 */
const LETTERS = new Parser(oState => {
    
    // Verify we have a state object and that state object is not current in "error" or at the end of the file
    if (oState && (oState.oError.bError || oState.bError)) {
        return oState;
    }

    // Define a simple regular expression that accepts only letters.
    const RE_LETTERS = /[a-zA-Z]+/;

    let sContent = fGetContent(oState);

    // Check to see if we got contents to test
    if (sContent !== null) {

        let oLetterResults = RE_LETTERS.exec(sContent);

        if (oLetterResults && oLetterResults[0] && oLetterResults.index === 0) {

            return fUpdateState(oState, oLetterResults[0]);
        }
        
        return fErrorState(oState, `LETTERS: Unable to find any alpha characters at start of the string`);
    }

    return fErrorState(oState, `LETTERS: Unexpected End of File`, true);
});

/**
 * LITERAL
 * Parser matches the start of the current content string, with the provided string literal
 * // Pass Through function
 * @param {string} sLiteral is the user defined string
 *  
 * // Parser Parameters
 * @param {object} oState Parser state object
 */
const LITERAL = sLiteral => new Parser(oState => {

    // Verify we have a state object and that state object is not current in "error" or at the end of the file
    if (oState && (oState.oError.bError || oState.bError)) {
        return oState;
    }

    // Verify a user defined string literal variable has something
    if (sLiteral) {

        if (typeof sLiteral !== "string") {

            return fErrorState(oState, `LITERAL: User string literal provided is not a string: instead recieved: ${typeof sLiteral}`);
        }

    }
    else {

        return fErrorState(oState, `LITERAL: No string literal was provided`, true);
    }

    // If we got here, we must have an expression, so get the contents
    let sContent = fGetContent(oState);

    if (sContent !== null) {

        if (sContent.startsWith(sLiteral)) {

            return fUpdateState(oState, sLiteral);
        }

        return fErrorState(oState, `LITERAL: Attempting to find ${sLiteral}, but found: ${sContent.slice(0, 20)}`);
    }

    return fErrorState(oState, `LITERAL: Unexpected End of File`, true);
});

/**
 * Node
 * Parser matchinbg more complext node like html and logic nodes
 * @param {string}      sNodeName       is used to name the node for debugging purposes
 * @param {reg exp}     reTagDetector   regular expression that should be used to identify the opening and closing tags for a given node
 * @param {class}       cTagParser      parser used to on the actual tags (if the node supports alternates, it will return alternate parses via the tag results in oNodeMeta)
 * @param {function}    fParseResults   function used to properly format the oNode result into a proper oAST strcuture
 * @param {class}       cContentParser  parse used to part the conditional contents, not the tags 
 * 
 * // Parser Parameters
 * @param {object} oState Parser state object
 */
// Nodes are detected and then there contents are parsed
const NODE = (sNodeName, cTagParser, cAlterParser) => new Parser (oState => {

    // Verify we have a state object and that state object is not current in "error" or at the end of the file
    if (oState && (oState.oError.bError || oState.bError)) {
        return oState;
    }

    const F_FIND_NODE_SEGMENTS = (oOpenTagState, sBlockContents) => {

        // Generate the segment array an prefill the first one
        var aSegments = [
            {
                sTagContents: oOpenTagState.vResult.sTagContents,
                sSegmentContents: null
            }
        ];

        let reAltTagChecker = fCloneRegExp(oOpenTagState.vResult.reAltTagDetectors, ['g']);
        let oAltTagCheck = null;
        
        let iLastAltTagIndex = 0;

        // Loop through and find all of the node alternative tags
        while(true) {

            // Take the remaining text chunks that have not been processed
            let sRemainingContent = sBlockContents.slice(iLastAltTagIndex);

            // Check and break out of the alt check look if there are no contents.
            if (sRemainingContent.length === 0) {
                break;
            }

            // Check to see if we can find the s block contents 
            oAltTagCheck = reAltTagChecker.exec(sRemainingContent);

            // Check to see if any alt tag was discovered
            if (oAltTagCheck !== null) {

                let sContentFromAltTag = sRemainingContent.slice(oAltTagCheck.index);

                // Parse the discovered alt tag
                let oAltTagState = cAlterParser.run(sContentFromAltTag);

                // Check to see if this is a valide alt tag, otherwise just error
                if (!oAltTagState.oError.bError) {

                    if (oAltTagState.vResult === null) {
                        return fErrorState(oOpenTagState, `NODE: Parser error on ${sNodeName} alternative tag: ${sContentFromAltTag.slice(0, 20)}`);
                    }

                    console.log("sSegmentContents");

                    // Update the last segment to have all the contents pri
                    let sSegmentContents = sRemainingContent.slice(0, oAltTagCheck.index);

                    let reSubLogicCheck = fCloneRegExp(oOpenTagState.vResult.reTagDetector, ['g'])

                    // Execute the sub logic check
                    let oSubLogicCheck = reSubLogicCheck.exec(sSegmentContents);

                    if (oSubLogicCheck !== null) {

                        console.log("Segment contains sublogic");

                    }
                    else {

                        // Assign contents to last segment object
                        aSegments[aSegments.length - 1].sSegmentContents = sSegmentContents;

                        // We have a valid alt tag (index or count to alt tag, plus alt tag length);
                        iLastAltTagIndex = oAltTagCheck.index + oAltTagState.iIndex;
                    }

                      
                }
                else {

                    return fErrorState(oOpenTagState, `NODE: Parser error on ${sNodeName} alternative tag: ${sContentFromAltTag.slice(0, 20)}`);
                }

            }
            else {

                // Append the last bit of contents to the segments array
                aSegments[aSegments.length -1].sSegmentContents = sRemainingContent;
                break;
            }

        }

        return fUpdateState(oOpenTagState, aSegments);
    };

    // Function is used to fine the proper node closing tag
    const F_FIND_NODE_CLOSING = (oOpenTagState) => {
        
        let reTagChecker = fCloneRegExp(oOpenTagState.vResult.reTagDetector, ['g']);
        let oTagCheck = null;
        let oClosingTagState = null;
        
        let iLastTagIndex = 0;
        let iOpenTag = 1;

        // Loop through and find the proper closing tag
        while (true) {

            // Execute check to see there is a matching tag can be found inside the current content string
            oTagCheck = reTagChecker.exec(oOpenTagState.sSource.slice(oOpenTagState.iIndex));

            if (oTagCheck !== null) {

                iLastTagIndex = oTagCheck.index;

                let sContentFromTag = oOpenTagState.sSource.slice(oOpenTagState.iIndex).slice(oTagCheck.index);

                // Re-execute the tag parser to see if the discovered tag is a closing tag
                oClosingTagState = cTagParser.run(sContentFromTag);

                // Check to see if we have a tag match
                if (!oClosingTagState.oError.bError) {

                    if (oClosingTagState.vResult.bOpenTag) {
                        iOpenTag += 1;
                    }
                    else {
                        iOpenTag -= 1;
                    }
    
                    if (iOpenTag === 0) {
                        break;
                    }
                }
                else {

                    return fErrorState(oOpenTagState, `NODE: Parser error on ${sNodeName} tag: ${sContentFromTag.slice(0, 20)}`);
                }

            }
            else {

                break;
            }

        }

        if (iOpenTag === 0) {

            // Add iContent length to the state as its going to be needed for nodes
            oClosingTagState.iContentLength = iLastTagIndex;

            return oClosingTagState;
        }
        else {

            return fErrorState(oOpenTagState, `NODE: Unable to find matching closing tag: `, true);
        }
    };

    const F_PARSE_TAG = (sContent, bSkipSegCheck) => {

        // Just Attemtp to parse the string
        let oOpenTagState = cTagParser.run(sContent);

        // Check to see if the parser successfully parse and is an open tat
        if (!oOpenTagState.oError.bError && oOpenTagState.vResult.bOpenTag) {

            // Check to see if this node is a block or inline node
            if (oOpenTagState.vResult.bBlockNode) {

                // Use the common internal function to find the node closing tag
                let oClosingTagState = F_FIND_NODE_CLOSING(oOpenTagState);

                // Verify the closing tag worked
                if (!oClosingTagState.oError.bError) {

                    // Pull the block content out.
                    let sBlockContents = oOpenTagState.sSource.slice(oOpenTagState.iIndex, (oOpenTagState.iIndex + oClosingTagState.iContentLength));

                    // Check to see if this tag supports alteratives
                    if (!bSkipSegCheck && oOpenTagState.vResult.reAltTagDetectors) {

                        let oSegmentsState = F_FIND_NODE_SEGMENTS(oOpenTagState, sBlockContents);

                        console.log("updated open alt state");

                        console.log(oSegmentsState);

                    }
                    else {

                    }

                }

                // Return the failing closing state
                return oClosingTagState;

            }
            else {

                console.log("This is an inline node!");
            }
            
        }

        return fErrorState(oOpenTagState, `NODE: `);
    };

    // If we got here, we must have an expression, so get the contents
    let sContent = fGetContent(oState);

    // Verify there is still content to run this parse agenst
    if (sContent !== null) {

        // Attempt to process the node from the current source contents!
        let oNodeState = F_PARSE_TAG(sContent);

        if (!oNodeState.oError.bError) {

            console.log("No errors with node!");

        }

        return fErrorState(oNodeState, oNodeState.oError.sError, true);
    }

    return fErrorState(oState, `NODE: Unexpected End of File`, true);
});

/**
 * REG_EXP
 * Parser uses provided user defined regular expression against source. Regular Expression only matches if the expression
 * index starts at 0.
 * // Pass Through function
 * @param {RegExp} reUserExpr is the user defined expresssion
 * 
 * // Parser Parameters
 * @param {object} oState Parser state object
 */
const REG_EXP = reUserExpr => new Parser(oState => {

    // Verify we have a state object and that state object is not current in "error" or at the end of the file
    if (oState && (oState.oError.bError || oState.bError)) {
        return oState;
    }

    // Verify a user defined expression variable has something
    if (reUserExpr) {

        if (typeof reUserExpr === "object") {

            if (!reUserExpr instanceof RegExp) {


                return fErrorState(oState, `REG_EXP: Expression provided was an object, but not a Regular Expression; instead recieved: ${reUserExpr.constructor} `, true);
            }

        }
        else {
        
            return fErrorState(oState, `REG_EXP: Expression provided was not a Regular Expression, instead recieved: ${typeof reUserExpr} `, true);
        }

    }
    else {

        return fErrorState(oState, `REG_EXP: No regular expression was provided`, true);
    }

    // If we got here, we must have an expression, so get the contents
    let sContent = fGetContent(oState);

    // Check to see if we got contents to test
    if (sContent !== null) {

        let oUserExprTest = reUserExpr.exec(sContent);

        if (oUserExprTest && oUserExprTest[0] && oUserExprTest.index === 0) {

            return fUpdateState(oState, oUserExprTest[0]);
        }

        return fErrorState(oState, `REG_EXP: Unable to find matching contents for expression: ${oUserExprTest}`);
    }

    return fErrorState(oState, `REG_EXP: Unexpected End of File`, true);
});

// =========================================================
// Utility Parsers
// These parsers are intended to build complex combinators.
// =========================================================

/**
 * BETWEEN
 * Utility parser used to isolate a parse content between two specific start and end parsers
 * // Pass Through function 1 - Start and end parser
 * @param {class}   cLeftParser     Parser
 * @param {class}   cRightParser    
 * 
 * // Pass Through function 2 - Content Parser
 * @param {class} cContentParser    Pasers to handles the inside contents 
 */
const BETWEEN = (cLeftParser, cRightParser) => (cContentParser) => SEQUENCE([
    cLeftParser,
    cContentParser,
    cRightParser
]).map((oResult) => {

    return oResult.vResult[1];
});

/**
 * CHOICE
 * Utility parser to choice the appropriate parser class that best fits the current context
 * // Pass Through function
 * @param {array}       acParsers array of all the parsers the content might match
 * @param {acParsers}   sChoiceName user provided name for the choice parser (only used for debuggin)
 * @param {boolean}     bRequireMatch if one of the provided parser needs to match
 * 
 * // Parser Parameters
 * @param {object} oState Parser state object
 */
const CHOICE = (sChoiceName, acParsers, bRequireMatch = true) => new Parser(oState => {

    if (Array.isArray(sChoiceName)) {
        acParsers = sChoiceName;
        sChoiceName = "Unamed Choice Parser";
    }

    // Verify we have a state object and that state object is not current in "error" or at the end of the file
    if (oState && (oState.oError.bError || oState.bError)) {
        return oState;
    }

    // Loop for all parsers one by one
    for (let cParser of acParsers) {

        let oNextState = cParser.fTransformer(oState);

        // Check to see if the oNextState was valie, if so return it
        if (!oNextState.oError.sError) {
            return oNextState;
        }

    }

    // See
    if (bRequireMatch) {        
        return fErrorState(oState, `CHOICE: ${sChoiceName} was unable to find a valid paser`);
    }
    else {
        return fErrorState(oState, `CHOICE: ${sChoiceName} was unable to find a valid paser`, true);
    }

});

/**
 * MANY
 * Utility Parser that will continuelly run the same parser of a
 * // Pass Through function
 * @param {class} cParser a user provided parser
 * 
 * // Parser Parameters
 * @param {object} oState Parser state object
 */
const MANY = cParser => new Parser(oState => {

    // Verify we have a state object and that state object is not current in "error" or at the end of the file
    if (oState && (oState.oError.bError || oState.bError)) {
        return oState;
    }

    // Pre-back the starting state
    let oNextState = oState;

    // Array to hold all results
    let aResults = [];

    // Loop until the provided class parser fails
    while (true) {

        // Execute the parser class and get the returned state
        let oTestState = cParser.fTransformer(oNextState);

        // Check to verify that the returned test state did not fail
        if (!oTestState.oError.bError) {

            // Check to see if the results is not null and add it to the array.
            // A null result is not a failure, its a way to just advance the parser.
            if (oTestState.vResult !== null) {

                aResults.push(oTestState.vResult);
            }
            
            // Change next state to be the current test state for next iteration
            oNextState = oTestState;

            // Double check to see if the current state is at the end of the file and break if it is.
            if (oNextState.bEoF) {
                break;
            }

        }
        else {

            // Break the while loop
            break;
        }

    }

    // Return the update state!
    return fUpdateState(oNextState, aResults);
});

/**
 * OPTIONAL
 * Utility Parser that will let a sub parser to return an error without causing the entire process end.
 * // Pass Through function
 * @param {class} cParser a user provided parser
 * 
 * // Parser Parameters
 * @param {object} oState Parser state object
 */
const OPTIONAL = cParser => new Parser(oState => {

    // Verify we have a state object and that state object is not current in "error" or at the end of the file
    if (oState && (oState.oError.bError || oState.bError)) {
        return oState;
    }

    let oNextState = cParser.fTransformer(oState);

    if (oNextState.oError.bError) {

        oState.vResult = null;

        // Parser failed, but thats ok we are just going to move on
        return oState;
    }

    return oNextState;
});

/**
 * SEQUENCE
 * Utility that loops through an array of Parsers in order, without repeating
 * // Pass Through function
 * @param {array} acParsers array of parser classes
 * @param {boolean} bReturnState toogle between returning the state object or just the results
 * // Parser Parameters
 * @param {object} oState Parser state object
 */
const SEQUENCE = (acParsers, bReturnState) => new Parser(oState => {

    if (oState.oError.bError || oState.bEoF) {
        return oState;
    }

    let aResults = [];

    let oNextState = oState;

    for (let cParser of acParsers) {

        oNextState = cParser.fTransformer(oNextState);

        if (oNextState.vResult !== null) {

            if (bReturnState) {

                aResults.push(oNextState);
            }   
            else{

                aResults.push(oNextState.vResult);
            }
        }

    }

    return fUpdateResult(oNextState, aResults);
});

const LAZY = parserThunk => new Parser(oState => {
    const cParser = parserThunk();
    return cParser.fTransformer(oState);
});

const THUNK = fParserThunk => new Parser(oState => {

    console.log(fParserThunk);

    const cParser = fParserThunk();

    console.log(cParser);

    return cParser.fTransformer(oState);
});

export {
    BETWEEN,
    CHOICE,
    EMPTY,
    LAZY,
    LETTERS,
    LITERAL,
    MANY,
    NODE,
    REG_EXP,
    OPTIONAL,
    SEQUENCE,
    THUNK
}