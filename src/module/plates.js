import {
    BETWEEN,
    CHOICE,
    EMPTY,
    LAZY,
    LETTERS,
    LITERAL,
    MANY,
    NODE,
    REG_EXP,
    OPTIONAL,
    SEQUENCE,
    THUNK
} from './lib/parser';

// Import the logic parsers
import {
    LOGIC_IF_STATEMENT_PARSER,
    LOGIC_IF_ALT_STATEMENT_PARSER
} from './_modules/LOGIC_IF';


const TEXT = REG_EXP(/^[\s\S][^<|{]+/).map(vResult => {

    console.log("text");
    console.log(vResult);

    return {
        "node": "text",
        "contents": vResult
    }
})

// Below are all the private rules plates will execute
const DOM_PARSER = THUNK(() =>
    MANY(
        CHOICE('Root Parser', [
            EMPTY,
            NODE(
                'LOGIC IF STATEMENT',
                LOGIC_IF_STATEMENT_PARSER,
                LOGIC_IF_ALT_STATEMENT_PARSER
            ),
            TEXT
        ])
    )
);

// Plates is the class object that pulls in all the parser ruls above to and simply exports the resulting AST
class Plates {

    constructor(sSourceString) {

        this.sSourceString = null;

        if (sSourceString && typeof sSourceString === "string" && sSourceString.length) {

            this.sSourceString = sSourceString;
        }
        else {

            throw new Error(`Plates: No content was provided!`);
        }

    }

    Parse() {

        let oAST = null;

        try {

            oAST = DOM_PARSER.run(this.sSourceString);

            /// Double check to see if the whole AST returned was just an error
            if (oAST && oAST.oError.bError) {

                // Throw the error
                throw new Error(oAST.oError.sError);    
            }

            return oAST;
        }
        catch(err) {

            console.log(err);
        }

    }

}

export default Plates;