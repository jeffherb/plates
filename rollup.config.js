export default [
	{
		input: 'src/module/plates.js',
		output: {
			file: 'dist/plates.js',
			format: 'esm'
		}
	},
	{
		input: 'src/module/plates.js',
		output: {
			file: 'dist/esm/plates.mjs',
			format: 'esm'
		}
	},
];