import Plates from '../src/module/plates';

// const TEST_STRING = `<!-- this is the by section -->
// `;

const TEXT_CONTEXT_STRING = `{{this.test}}`;

//const LOGIC_NODE_BLOCK = `{{#if this.checked}} true{{#if this.misCheck}}true{{else}}true{{/if}}{{else}}false{{/if}}`

const LOGIC_NODE_BLOCK = `{{#if this.checked}}true{{else}}false{{/if}}`

//const TEST_STRING = `<p>Test</p>`;

const PLATES = new Plates(LOGIC_NODE_BLOCK);

let oPlatesAST = PLATES.Parse();

console.log("Finished oAST!");
console.log(oPlatesAST);

console.log("Expected output!");
console.log(JSON.stringify(oPlatesAST.vResult, null, 4));
