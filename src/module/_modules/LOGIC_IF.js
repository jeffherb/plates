import {
    BETWEEN,
    CHOICE,
    EMPTY,
    LAZY,
    LETTERS,
    LITERAL,
    MANY,
    NODE,
    REG_EXP,
    OPTIONAL,
    SEQUENCE,
    THUNK
} from '../lib/parser';

const LOGIC_QAULITFIER = BETWEEN(
    LITERAL('{{'),
    LITERAL('}}')
);

const LOGIC_IF_NODE_BUILDER = (oOpenState, oCloseState, aParts, cContentParser) => {

    console.log("In node Builder function!");

    let oNode = {
        type: "if",
        node: "logic",
        conditionals: [],
        alternative: null
    };

    for (let oPart of aParts) {

        console.log(oPart);

    }

    console.log(oOpenState);
    console.log(oCloseState);

    return oNode;
};

const LOGIC_IF_ALT_STATEMENT_PARSER = LOGIC_QAULITFIER(
    SEQUENCE([
        OPTIONAL(EMPTY),
        REG_EXP(/else|elseif/),
        OPTIONAL(EMPTY),
        OPTIONAL(REG_EXP(/([^}}]+)/))
    ])
).map(vResult => {

    // Pull the right context from the Choice Sub.
    vResult = vResult.vResult[0];

    let oNodeAltMeta = {
        vResult: vResult,
        bConditional: false,
        sTagType: null,
        sTagContents: null
    };

    if (vResult[0] === "elseif") {
        oNodeAltMeta.bConditional === true;
        oNodeAltMeta.sTagContents = vResult[1];
    }

    console.log("Alt Map:");
    console.log(oNodeAltMeta);

    return oNodeAltMeta;
});

const LOGIC_IF_STATEMENT_PARSER = LOGIC_QAULITFIER(
    SEQUENCE([
        OPTIONAL(EMPTY),
        CHOICE('IF STATEMENT BREAKDOWN',[
            SEQUENCE([
                OPTIONAL(EMPTY),
                REG_EXP(/\#|\//),
                REG_EXP(/if/i),
                OPTIONAL(EMPTY),
                OPTIONAL(REG_EXP(/([^}}]+)/))
            ]),
            SEQUENCE([
                REG_EXP(/elseif|else/i),
                OPTIONAL(EMPTY),
                OPTIONAL(REG_EXP(/([^}}]+)/))
            ])
        ]),
        OPTIONAL(EMPTY),
    ])
).map(vResult => {

    // Pull the right context from the Choice Sub.
    vResult = vResult.vResult[0];

    // Defines the extra information only needed once the starting tag is discoverd
    let oNodeMeta = {
        vResult: vResult,
        sType: null,
        sTagName: null,
        sTagContents: null,
        bNodeTag: false,    // Indicates this tag is an opening or closing tag
        bOpenTag: false,
        bBlockNode: true,
        reTagDetector: /\{\{(?:\#|\/)+if/g,
        reAltTagDetectors: /{{else/g,
    };

    // Check to see if this tag is a start of end node tag
    if (vResult[0] === "#" || vResult === "/") {

        oNodeMeta.bNodeTag = true
        oNodeMeta.sTagName = vResult[1];

        // Check indicator symbol to determine if this is a open or closing tag
        if (vResult[0] === "#") {
            oNodeMeta.bOpenTag = true;
            oNodeMeta.sType = "opening";
            oNodeMeta.sTagContents = vResult[2];
        }
        else {
            
            oNodeMeta.sType = "closing";
            oNodeMeta.sTagContents = false;
        }

    }

    return oNodeMeta;
});

export {
    LOGIC_IF_STATEMENT_PARSER,
    LOGIC_IF_ALT_STATEMENT_PARSER
}